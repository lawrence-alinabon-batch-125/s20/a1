/*
	Activity

	Create an array of objects called courses:

		Each object should have the following fields
		
		id - string
		name - string
		description - string
		price - string
		isActive - boolean

	Thea aray should have a minimum of 4 objects

	Create
		- Create an arrow function call addCourse which allows us to add a new object into an aray. This function should recieve the following the data:

			-id
			-name
			-description
			-price
			-isActive
		-This function should be able to show an alert after adding into an array
			`You have created <nameOfCourse>. Its price is <priceOfCourse>`

	Retreive/Read
		- Create an arrow function getSingleCourse which allows us to find a particular course by the provideing the coruse's id.
			- Show the details of the found course in the console.
			- return the found course

		- Create an arrow function getAllCourses which is able to show all of the items/objects in the array in our console.
			- return the courses array

	Update 
		- Create an arrow function call archiveCourse which is able to upadte/re-assign the isActive Property of a particular course. This function should be able to recieve the particular index number of the item as an argument.

			- show the updated course in the console.

	Delete 
		- Create an arrow function called deleteCourse which is able to delete the last course object in the array.


	Stretch Goals:

	Create an arrow function which can show all of the active courses only. Show the active courses in the console See .filter

	In the archiveCourse function, the function should instead receive the name of the course and use the name to find the index number of the course see findIndex()

*/


let courses = [
	{
		id : `1`,
		name : `HTML`,
		description : `HTML Basics`, 
		price : 1500,
		isActive :true

	},
	{
		id : `2`,
		name : `CSS`,
		description : `CSS Basics`, 
		price : 1500,
		isActive :false

	},
	{
		id : `3`,
		name : `Javascript`,
		description : `Javascript Programming`, 
		price : 1500,
		isActive :true

	},
	{
		id : `4`,
		name : `JQuery`,
		description : `JQuery`, 
		price : 1500,
		isActive :true

	}
]

// console.log(courses)

const addCourse = (id, name, description, price, isActive) => {
 	courses.push({id, name, description,price,isActive}) 
 	alert(`You have created ${name}. It's price is ${price}`);

}

// addCourse('5',`Java`, `Java`, 5000, true);

const getSingleCourse = (id) => console.log(courses.find((course) => course.id === id));

const getAllCourse = () => console.log(courses);

const archiveCourse = (id) => {
	let courseid = courses.findIndex((course) => course.id === id)

	if(courses[courseid].isActive === true){
		courses[courseid].isActive = false;
		console.log(courses[courseid])
	}else{
		courses[courseid].isActive = true;
		console.log(courses[courseid])
	}
}

const deleteCourse = () => courses.pop();

const getActive = () => console.log(courses.filter((course) => course.isActive === true))

const archiveCourse2 = (name) => {
	let courseid = courses.findIndex((course) => course.name=== name)

	if(courses[courseid].isActive === true){
		courses[courseid].isActive = false;
		console.log(courses[courseid])
	}else{
		courses[courseid].isActive = true;
		console.log(courses[courseid])
	}
}